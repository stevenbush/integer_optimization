/**
 * Created by jiyuanshi on 4/14/15.
 */

import com.jom.*;

public class Test {
    public static void main(String[] args) {

        double[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        DoubleMatrixND matrixND = new DoubleMatrixND(new int[]{3, 3}, values);
        System.out.println(matrixND);

        OptimizationProblem op = new OptimizationProblem();
        op.setInputParameter("m", matrixND);
        System.out.println("sum(m,1)=" + op.parseExpression("sum(m,1)").evaluate());
        System.out.println("sum(m,2)=" + op.parseExpression("sum(m,2)").evaluate());

    }
}
